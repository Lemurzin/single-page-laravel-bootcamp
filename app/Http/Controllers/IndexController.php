<?php

namespace App\Http\Controllers;

use App\Models\Card;

class IndexController extends Controller
{
    public function index()
    {
        $data = Card::orderByDesc('visible')->orderBy('position')->where('visible', true)->get();

        $data->count() % 2 ? $odd = $data->count() - 1 : $odd = null;

        return view('index',compact('data', 'odd'));
    }
}
