<?php

namespace App\Http\Controllers\Card;

use Carbon\Carbon;
use App\Models\Card;
use App\Models\Image;
use App\Http\Resources\CardResource;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Card\StoreRequest;
use App\Http\Requests\Card\UpdateRequest;
use App\Http\Controllers\Card\BaseController;


class CardController extends BaseController
{
    public function index()
    {
        $data = Card::orderByDesc('visible')->orderBy('position')->paginate(10);
        return inertia('Cards/Index', [
            'title' => 'Cards',
            'cards' => CardResource::collection($data->items()),
            'links' =>  json_decode(json_encode($data))->links,
        ]);
    }

    public function store(StoreRequest $request)
    {
        $data = $request->validated();
        
        $this->service->store($data);

        return redirect()->back();
    }

    public function update(UpdateRequest $request)
    {
       
        $data = $request->validated();

        $this->service->update($data);

        return redirect()->back();
    }

    public function destroy(Card $card)
    {
        $this->service->delete($card);

        return redirect()->back();
    }
}
