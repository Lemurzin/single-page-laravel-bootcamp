<?php

namespace App\Http\Controllers\Card;

use App\Services\CardService;
use App\Http\Controllers\Controller;

class BaseController extends Controller
{
    public $service;

    public function __construct(CardService $service)
    {
        $this->service = $service;
    }
}