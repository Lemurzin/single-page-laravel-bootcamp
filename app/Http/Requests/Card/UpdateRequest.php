<?php

namespace App\Http\Requests\Card;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'id_card' => 'required|integer|exists:cards,id',
            'title' => 'required|string',
            'subtitle' => 'required|string',
            'images' => 'nullable|array',
            "names.*"  => "nullable|mimes:jpg,jpeg,png",
            'comment' =>  'nullable|string',
            'position' => 'nullable|integer',
            'visible' =>  'boolean',
            'image_ids_for_delete' => 'nullable|array',
        ];
    }
}
