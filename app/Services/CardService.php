<?php

namespace App\Services;

use Exception;
use Carbon\Carbon;
use App\Models\Card;
use App\Models\Image;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class CardService{

    public function store($data)
    {
        $newImages = array();

        try
        {
            DB::beginTransaction();
            
            $images = $data['images'];
            unset($data['images']);

            if(!empty($images)){
                foreach ($images as $image){
                    $filePath = $this->createImage($image);
                    array_push($newImages, $filePath);
                }

                $card = Card::create($data);
                
                foreach($newImages as $newImage)
                {
                    Image::create([
                        'path' => $newImage,
                        'card_id' => $card->id,
                    ]);
                }
            }

            DB::commit();
        }catch(Exception $exception)
        {
            $this->deleteImagesFromStorage($newImages);

            DB::rollback();
            abort(500);
        }
    }

    public function update($data)
    {
        $imageIdsForDelete = $data['image_ids_for_delete'];
        unset($data['image_ids_for_delete']);
        $imagesPathsForDeleteFromStorage = array();

        $images = $data['images'];
        unset($data['images']);
        $newImages = array();

        try
        {
            DB::beginTransaction();

            $card = Card::find($data['id_card']);
            unset($data['id_card']);

            if(!empty($imageIdsForDelete)){
                $currentImages = $card->images;
                foreach($currentImages as $currentImage){
                    if(in_array($currentImage->id, $imageIdsForDelete)){
                        array_push($imagesPathsForDeleteFromStorage, $currentImage->path);
                        $currentImage->delete();
                    }
                }
            }

            if(!empty($images)){
                foreach ($images as $image){
                    $filePath = $this->createImage($image);
                    array_push($newImages, $filePath);
                }

                foreach($newImages as $newImage){
                    Image::create([
                        'path' => $newImage,
                        'card_id' => $card->id,
                    ]);
                }
            }

            $card->update($data);
            DB::commit();
        } catch(Exception $exception)
        {
            $this->deleteImagesFromStorage($newImages);

            DB::rollback();
            abort(500);
        } finally 
        {
            $this->deleteImagesFromStorage($imagesPathsForDeleteFromStorage);
        }
    }

    public function delete($card)
    {
        $this->deleteImagesFromStorage($this->getPaths($card->images));
     
        $card->delete();
    }

    private function getPaths($images): array
    {
        $paths = array();
        foreach($images as $image){
            array_push($paths, $image->path);
        }
        return $paths;
    }
    private function deleteImagesFromStorage(array $paths)
    {
        foreach($paths as $path){
            if (Storage::disk('public')->exists($path)) 
            {
                Storage::disk('public')->delete($path);
            }
        }
    }

    private function createImage($image)
    {
        $name = md5(Carbon::now().'_'.$image->getClientOriginalName()).'.'.$image->getClientOriginalExtension();
        $filePath = Storage::disk('public')->putFileAs('/images', $image, $name);
        return '/'.$filePath;
    }
}