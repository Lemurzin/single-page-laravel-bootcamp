import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';
import vue from '@vitejs/plugin-vue';

export default defineConfig({
    plugins: [
        laravel({
            input: [
                'resources/js/app.js',
                'resources/css/app.css',
                'resources/css/bootstrap5.css',
                'resources/js/bootstrap5.js',
            ],
            refresh: true,
        }),
        vue({
            template: {
                transformAssetUrls: {
                    base: null,
                    includeAbsolute: false,
                },
                //temp
                compilerOptions: {
                    isCustomElement: (tag) => tag.startsWith('X-')
                },
            },
        }),
    ],
});
