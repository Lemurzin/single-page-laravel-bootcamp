<?php 

return [
    'only' => [
        'admin.users.*',
        'admin.cards.*',
        'dashboard',
        'logout',
        'login',
        'index'
    ],
];