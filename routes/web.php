<?php

use Inertia\Inertia;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Application;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\Card\CardController;
use App\Http\Controllers\User\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix'=>'admin','as'=>'admin.','middleware'=>['auth','admin']], function(){
    Route::get('/', function () {
        return redirect()->action([CardController::class, 'index']);
    });
    Route::post('/cards/update', [CardController::class, 'update'])->name('cards.update');
    Route::resource('cards', CardController::class)
        ->only('index', 'store', 'destroy');
    Route::resource('users', UserController::class)
        ->only('index', 'store', 'destroy', 'update');
});

Route::get('/', [IndexController::class, 'index'])->name('index');

Route::get('/dashboard', function () {
    return redirect()->route('index');
})->name('dashboard');

require __DIR__.'/auth.php';
