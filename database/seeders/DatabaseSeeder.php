<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->deleteFilesStorage();
        \App\Models\User::create([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('admin'),
            'isAdmin' => true
        ]);
        \App\Models\User::factory(11)->create();
        \App\Models\Card::factory(11)->create();
        \App\Models\Image::factory(25)->create();
    }

    private function deleteFilesStorage()
    {
        $files = Storage::disk('public')->allFiles('images/');
        Storage::disk('public')->delete($files);
    }
}
