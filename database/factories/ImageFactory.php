<?php

namespace Database\Factories;

use App\Models\Card;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Image>
 */
class ImageFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $img = fake()->image('public/storage/images',640,480, null, false);
        dump("create file: storage/images/".$img);

        return [
            'path' => '/images/'.$img,
            'card_id' => Card::get()->random()->id,
        ];
    }
}
