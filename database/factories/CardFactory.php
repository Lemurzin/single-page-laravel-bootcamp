<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Card>
 */
class CardFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    private static $order = 1;
    public function definition(): array
    {
        return [
            'title' => fake()->word(),
            'subtitle' => fake()->text(),
            'comment' => fake()->word(),
            'position' => self::$order++,
            'visible' => rand(0,1),
        ];
    }
}
