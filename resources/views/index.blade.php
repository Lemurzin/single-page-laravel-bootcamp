<!DOCTYPE html>
<html lang="ru">
<head>
    @include('index.head')
</head>
<body>
  @include('index.metrika')



  <div class="bgr">
    <div>
      <div style="height: 100vh;" class="d-flex flex-column justify-content-center">
        <header class="d-flex justify-content-center">
          <div class="container d-flex justify-content-center flex-column"> 
            <div class="d-flex justify-content-between">
              <div class="logo d-flex">
                <img src="{{asset('/src/ico.png')}}">
              </div>
              <div class="d-flex">
                <div class="d-flex justify-content-center header__icons">
                  <div class="d-flex justify-content-center">
                    <ul class="nav nav-pills">
                      <li class="nav-item mx-2 d-flex justify-content-center flex-column">
                          <a href="#####">
                            <i class="bi bi-telegram link_color link_color-active"></i>
                          </a>
                      </li>
                      <li class="nav-item mx-2 tel-i col-center">
                        <a href="tel:#####">
                          <i class="bi bi-telephone link_color link_color-active"></i>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="telephone col-center">
                  <div class="d-flex justify-content-end">
                    <a href="tel:+#####" class="link_color link_color-active">
                      +#####
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </header>
        <div class="main-container container main d-flex flex-column justify-content-center" style="flex:1;">
          <h1 class="text-center">#####</h1>
          <h2 class="text-center">#####</h2>
          <br>
          <h2 class="text-center">#####</h2> 
          <h2 class="text-center">#####</h2>
          <h2 class="text-center">#####</h2>
          <h2 class="text-center">#####</h2>
          <h2 class="text-center">#####</h2>
        </div>
      </div>
    
      <main>
        <div class="main-container container">
          <div class="products">
            <div class="row p-3 row-carts">
                @foreach ($data as $keyCard => $card)
                  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 {{$keyCard === $odd ? 'offset-md-3' : ''}}">
                    <div class="card mb-3">
                      <div id="carouselExampleControls{{$keyCard}}" class="carousel slide" data-bs-ride="carousel">
                          <div class="carousel-inner">
                            @foreach ($card->images as $keyImg => $img)
                              <div class="carousel-item {{$keyImg === 0 ? 'active' : ''}}">
                                <img src="{{'storage/'.$img->path}}" class="d-block w-100" alt="...">
                              </div>
                            @endforeach
                          </div>
                          @if ($card->images->count() > 1)
                            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls{{$keyCard}}" data-bs-slide="prev">
                              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                              <span class="visually-hidden">Previous</span>
                            </button>
                            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls{{$keyCard}}" data-bs-slide="next">
                              <span class="carousel-control-next-icon" aria-hidden="true"></span>
                              <span class="visually-hidden">Next</span>
                            </button>
                          @endif
                      </div>
                      <div class="card-body">
                        <h5 class="card-title text-center">{{$card->title}}
                          <p class="card-text text-center"><small class="text-muted">{{$card->comment}}</small></p>
                        </h5>
                        <p class="card-text text-center">{{$card->subtitle}}</p>
                      </div>
                    </div>
                  </div>
                @endforeach
            </div>
          </div>
        </div>
      </main>

      <footer>
        <div class="container">
          <div class="d-flex flex-wrap justify-content-between align-items-center py-3">
            <ul class="nav nav-pills">
              <li class="nav-item mx-2">
                  <a href="#####">
                    <i class="fs-3 bi bi-telegram link_color link_color-active"></i>
                  </a>
              </li>
            </ul>
      
            <div class="col d-flex align-items-center py-3">
                <span class="copiratef text-muted" style="color: rgb(255, 255, 255) !important">© 2021-2023</span>
            </div>
          
            <div class="telephone col d-flex flex-column justify-content-center">
              <div class="d-flex justify-content-end">
                <a href="tel:#####" class="fs-4 link_color link_color-active">
                  #####
                </a>
              </div>
            </div>
              
          </div>
          <div class="row">
            <div class="col text-center copirates">
              <span style="color: rgb(255, 255, 255) !important" class="text-muted">© 2021-2023</span>
            </div>
          </div>
        </div>
      </footer>
    </div>

  </div>

  @vite('resources/js/bootstrap5.js')
</body>
</html>