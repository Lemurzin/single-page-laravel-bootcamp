<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Cache-Control" content="max-age=86400, must-revalidate">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>#####</title>

<meta name="description" content="#####">
<meta name="image" content="{{asset('/src/ico/ico152.png')}}">
<meta property="og:type" content="website">
<meta property="og:title" content="#####">
<meta property="og:description" content="#####">
<meta property="og:image" content="#####">
<meta property="og:url" content="#####">
<link rel="canonical" href="#####">
<link rel="apple-touch-icon" sizes="57x57" href="{{asset('/src/ico/ico57.png')}}">
<link rel="apple-touch-icon" sizes="60x60" href="{{asset('/src/ico/ico60.png')}}">
<link rel="apple-touch-icon" sizes="72x72" href="{{asset('/src/ico/ico72.png')}}">
<link rel="apple-touch-icon" sizes="76x76" href="{{asset('/src/ico/ico76.png')}}">
<link rel="apple-touch-icon" sizes="114x114" href="{{asset('/src/ico/ico114.png')}}">
<link rel="apple-touch-icon" sizes="120x120" href="{{asset('/src/ico/ico120.png')}}">
<link rel="apple-touch-icon" sizes="144x144" href="{{asset('/src/ico/ico144.png')}}">
<link rel="apple-touch-icon" sizes="152x152" href="{{asset('/src/ico/ico152.png')}}">
<link rel="apple-touch-icon" sizes="180x180" href="{{asset('/src/ico/ico180.png')}}">
<link rel="icon" type="image/png" sizes="192x192"  href="{{asset('/src/ico/ico192.png')}}">
<link rel="icon" type="image/png" sizes="32x32" href="{{asset('/src/ico/ico32.png')}}">
<link rel="icon" type="image/png" sizes="96x96" href="{{asset('/src/ico/ico96.png')}}">
<link rel="icon" type="image/png" sizes="16x16" href="{{asset('/src/ico/ico16.png')}}">
<meta name="msapplication-TileImage" content="">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="theme-color" content="#ffffff">
<meta name="apple-mobile-web-app-title" content="#####">
<meta name="apple-mobile-web-app-capable" content="no">
<meta name="format-detection" content="telephone=no">

<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
@vite('resources/css/bootstrap5.css')

<link rel="stylesheet" href="{{asset('/src/index.css')}}">


